# This file is part of the default_value module for Tryton.
# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
try:
    from trytond.modules.default_value.tests.test_default_value import suite
except ImportError:
    from .test_default_value import suite

__all__ = ['suite']
