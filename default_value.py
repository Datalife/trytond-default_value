# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from sql.conditionals import Coalesce
from sql.operators import Equal
from datetime import date, datetime, time
from decimal import Decimal
from trytond import backend
from trytond.model import ModelView, ModelSQL, fields, Exclude
from trytond.pool import Pool
from trytond.pyson import Bool, Eval
from trytond.rpc import RPC
from trytond.transaction import Transaction
from trytond.i18n import gettext
from trytond.exceptions import UserError, UserWarning


_FIELD_TYPES = ['boolean', 'char', 'integer', 'text', 'float', 'numeric',
    'date', 'datetime', 'time', 'many2one', 'selection', 'reference']


class DefaultValue(ModelSQL, ModelView):
    'Default Value'
    __name__ = 'default.value'

    model = fields.Many2One('ir.model', 'Model', required=True,
        states={
            'readonly': Bool(Eval('default_value')),
            }, depends=['default_value'])
    field = fields.Many2One('ir.model.field', 'Field', required=True,
        domain=[
            ('model', '=', Eval('model', 0)),
            ('ttype', 'in', _FIELD_TYPES),
            ], depends=['model', 'default_value'],
        states={
            'readonly': Bool(Eval('default_value')),
            })
    field_type = fields.Function(fields.Char('Field Type', readonly=True,
            states={
                'invisible': ~Bool(Eval('field')),
                }, depends=['field']),
        'on_change_with_field_type')
    default_value = fields.Char('Default Value', readonly=True)
    boolean = fields.Function(fields.Boolean('Value',
            states={
                'invisible': Eval('field_type') != 'boolean',
                }, depends=['field_type']),
        'get_value', setter='set_value')
    char = fields.Function(fields.Char('Value',
            states={
                'invisible': Eval('field_type') != 'char',
                }, depends=['field_type']),
        'get_value', setter='set_value')
    integer = fields.Function(fields.Integer("Value",
            states={
                'invisible': Eval('field_type') != 'integer',
                }, depends=['field_type']),
        'get_value', setter='set_value')
    text = fields.Function(fields.Text('Value',
            states={
                'invisible': Eval('field_type') != 'text',
                }, depends=['field_type']),
        'get_value', setter='set_value')
    float = fields.Function(fields.Float('Value',
            states={
                'invisible': Eval('field_type') != 'float',
                }, depends=['field_type']),
        'get_value', setter='set_value')
    numeric = fields.Function(fields.Numeric('Value',
            states={
                'invisible': Eval('field_type') != 'numeric',
                }, depends=['field_type']),
        'get_value', setter='set_value')
    date = fields.Function(fields.Date('Value',
            states={
                'invisible': Eval('field_type') != 'date',
                }, depends=['field_type']),
        'get_value', setter='set_value')
    datetime = fields.Function(fields.DateTime('Value',
            states={
                'invisible': Eval('field_type') != 'datetime',
                }, depends=['field_type']),
        'get_value', setter='set_value')
    time = fields.Function(fields.Time('Value',
            states={
                'invisible': Eval('field_type') != 'time',
                }, depends=['field_type']),
       'get_value', setter='set_value')
    many2one = fields.Function(fields.Selection('get_selection_values',
            'Value', states={
                'invisible': Eval('field_type') != 'many2one',
                }, depends=['field_type']),
        'get_value', setter='set_value')
    selection = fields.Function(fields.Selection('get_selection_values',
            'Value', states={
                'invisible': Eval('field_type') != 'selection',
                }, depends=['field_type']),
        'get_value', setter='set_value')
    reference = fields.Function(fields.Char('Value',
            states={
                'invisible': Eval('field_type') != 'reference',
                }, depends=['field_type']),
        'get_value', setter='set_value')
    user = fields.Many2One('res.user', 'User',
        states={
            'readonly': Bool(Eval('default_value'))},
        depends=['default_value'])

    @classmethod
    def __setup__(cls):
        super(DefaultValue, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints = [
            ('uniq_field_user',
                Exclude(t, (t.field, Equal), (Coalesce(t.user, -1), Equal)),
                'default_value.msg_field_user_unique'),
        ]
        cls.__rpc__.update({
                'get_selection_values': RPC(instantiate=0),
                })

    @classmethod
    def __register__(cls, module_name):
        super(DefaultValue, cls).__register__(module_name)
        table = cls.__table_handler__(module_name)

        table.drop_constraint('default_uniq')

    @fields.depends('field')
    def get_selection_values(self, name=None):
        pool = Pool()
        field = self.field
        model = field and field.model or False
        selection = [(None, '')]
        if model and field:
            if self.on_change_with_field_type() == 'selection':
                Model = pool.get(model.model)
                selection.extend(Model()._fields[field.name].selection)
            elif self.on_change_with_field_type() == 'many2one':
                Model = pool.get(field.relation)
                selection.extend((str(m.id), m.rec_name)
                    for m in Model.search([]))
        return selection

    @classmethod
    def create(cls, vlist):
        pool = Pool()
        Warning = pool.get('res.user.warning')

        Field = pool.get('ir.model.field')
        for val in vlist:
            field, = Field.search([('id', '=', val['field'])])
            model = pool.get(field.model.model)
            if hasattr(model, 'default_%s' % field.name):
                key = 'field_has_default_value_%s' % val['field']
                if Warning.check(key):
                    raise UserWarning(key, gettext(
                        'default_value.msg_has_default_value',
                        fieldname=field.name))
            if isinstance(model._fields[field.name], fields.Function):
                raise UserError(gettext(
                    'default_value.msg_is_functional',
                    fieldname=field.name))
        return super(DefaultValue, cls).create(vlist)

    @fields.depends('field')
    def on_change_with_field_type(self, name=None):
        return self.field.ttype if self.field else None

    @fields.depends('field_type', 'default_value')
    def get_value(self, name=None):
        if not name or self.field_type == name:
            value = self.default_value
            if self.field_type == 'boolean':
                return True if value == 'True' else False
            elif self.field_type == 'integer':
                return int(value) if value else 0
            elif self.field_type == 'float':
                return float(value) if value else 0.0
            elif self.field_type == 'numeric':
                return Decimal(value) if value else Decimal('0.0')
            elif self.field_type == 'date':
                return (value and
                    date(int(value[:4]), int(value[5:7]), int(value[8:])
                        ) or None)
            elif self.field_type == 'datetime':
                return (value and
                    datetime.strptime(value, '%Y-%m-%d %H:%M:%S') or None)
            elif self.field_type == 'time':
                return (value and
                    time(int(value[:2]), int(value[3:5]), int(value[6:])
                        ) or None)
            return value

    @classmethod
    def set_value(cls, default_values, name, value):
        to_write = []
        for default_value in default_values:
            if name == default_value.field_type and value is not None:
                to_write.append(default_value)
        if to_write:
            cls.write(to_write, {'default_value': str(value)})

    def get_rec_name(self, name):
        if self.user:
            return '%s [%s]' % (self.model.rec_name, self.user.rec_name)
        return self.model.rec_name


class DefaultValueMixin(object):
    """Mixin class to extend default_get method"""

    @classmethod
    def default_get(cls, fields_names, with_rec_name=True):
        pool = Pool()
        Defvalue = pool.get('default.value')

        values = super(DefaultValueMixin, cls).default_get(fields_names,
            with_rec_name=with_rec_name)

        defvalues = Defvalue.search([
            ('model.model', '=', cls.__name__),
            ('field.name', 'in', fields_names),
            ['OR',
                ('user', '=', None),
                ('user', '=', Transaction().user)]
        ], order=[('field', 'ASC'), ('user', 'ASC NULLS LAST')])

        _fields = set()
        for defvalue in defvalues:
            if defvalue.field.id in _fields:
                continue
            _fields.add(defvalue.field.id)
            value = defvalue.get_value()
            if defvalue.field.ttype == 'many2one' and value:
                Target = pool.get(defvalue.field.relation)
                target_record = Target(int(value))
                value = target_record.id

                if with_rec_name and 'rec_name' in Target._fields:
                    field_name = '%s.' % defvalue.field.name
                    if (field_name in values
                            and 'rec_name' in values[field_name]):
                        values[field_name]['rec_name'] = target_record.rec_name
                    else:
                        values.setdefault(field_name, {})
                        values[field_name]['rec_name'] = target_record.rec_name
            values[defvalue.field.name] = value
        return values
