# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool
from trytond.model import Model
from . import default_value


def register():
    Pool.register_mixin(
        default_value.DefaultValueMixin,
        module='default_value', classinfo=Model)
    Pool.register(
        default_value.DefaultValue,
        module='default_value', type_='model')
